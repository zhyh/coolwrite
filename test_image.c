/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%                                                            %%%%%%
%%%%%%       欢迎到www.opencvchina.com下载源代码和资料              %%%%%%
%%%%%%                                                            %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/
/*
 * Code written by Lya (GeckoGeek.fr)
 */

#include "opencv/highgui.h"
#include "opencv/cv.h"
 
//#include"linux_serial.h"
#include <stdlib.h>
#include <stdio.h>
#include     <unistd.h>     /*Unix标准函数定义*/
 /*the time server using shared memory */

#include<stdio.h>
#include<sys/shm.h>
#include<time.h>
#include<signal.h>

#define TIME_MEM_KEY 99                  //共享内存 号
#define SEG_SIZE ((size_t)10)
#define oops(m,x) {perror(m); exit(x);}



// Maths methods
#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))  
#define abs(x) ((x) > 0 ? (x) : -(x))
#define sign(x) ((x) > 0 ? 1 : -1)
 
// Step mooving for object min & max
#define STEP_MIN 5
#define STEP_MAX 100 

//#define flag 1  
#define noflag 0  
  
//串口终端接收
//char buf1[255];
//int res = 0;

int seg_id;

int move_flag = 0;
int line_flag = 0;
int temp_fd = 0;

// FILE *fp_color = NULL; //采样颜色指针
 
IplImage *image;
IplImage *temp;
IplImage *remain_image;
IplImage* img2;
 
// Position of the object we overlay
CvPoint objectPos;
	// Next position of the object we overlay
	CvPoint objectNextPos;
CvPoint center;
// Color tracked and our tolerance towards it
 int h = 0, s = 0, v = 0, tolerance = 10;
/*
 * Get the color of the pixel where the mouse has clicked
 * We put this color as model color (the color we want to tracked)
 */
/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%                                                            %%%%%%
%%%%%%       欢迎到www.opencvchina.com下载源代码和资料              %%%%%%
%%%%%%                                                            %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


*/

 
int main() {


	int  *mem_ptr;    //共享内存
	
    long fpos;    //临时文件偏移量
    int write_num= 0;  //写入字节数
    
    FILE *fp = NULL; //需要注意
    
    fp = fopen("/tmp/serial", "w+");
    temp_fd = fileno(fp);
    
    if(NULL == fp)
    {
        return -1; //要返回错误代码
    }
  	
	// Image & hsvImage
	IplImage *hsv;

	// Key for keyboard event
	char key;
 
	// Number of tracked pixels
	int nbPixels;

 
	// Initialize the video Capture (200 => CV_CAP_V4L2)
	img2=cvCreateImage(cvSize(640,480),IPL_DEPTH_32F,3);  //my code
	
	// Create the windows
	cvNamedWindow("my window",CV_WINDOW_AUTOSIZE); //my code
	cvSetZero(img2);
	// While we don't want to quit
		
		while(1)
		{
		//fscanf(fp, "%d %d %d", &objectPos.x, &objectPos.y, &v);
			//if (v = 1)
				cvDrawCircle(img2, objectPos, 8, CV_RGB(255, 0, 0), -1, 8 , 0);
					cvShowImage("my window", img2);
					cvDrawCircle(img2, objectPos, 8, CV_RGB(255, 0, 0), -1, 8 , 0);
		}

		// We wait 10 ms
		key = cvWaitKey(10000);
 
	// Destroy the windows we have created
	cvDestroyWindow("my window");
 
	// Destroy the capture
 	fclose(fp);		//关闭文件描述符
 	//fclose(fp_color);
	return 0;
 
}


