/*
 * sound.c
 */
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <linux/soundcard.h>
#include<math.h>
#include"fft.h"

//#include"sound.h"
#include<sys/shm.h>
#include<time.h>
#include<signal.h>

#define TIME_MEM_KEY 99
#define SEG_SIZE ((size_t) 10)
#define oops(m,x) {perror(m); exit(x);}

#define LENGTH 0.1    /* 存储秒数 */
#define RATE 22050   /* 采样频率 */
#define SIZE 8      /* 量化位数 */
#define CHANNELS 1  /* 声道数目 */
/* 用于保存数字音频数据的内存缓冲区 */

//unsigned char buf[LENGTH*RATE*SIZE*CHANNELS/8];
unsigned char buf[1000];
	int seg_id;
	
int main()
{
  int fd;	/* 声音设备的文件描述符 */
  int arg;	/* 用于ioctl调用的参数 */
  int status;   /* 系统调用的返回值 */
  int i = 0;
  int value = 0;
  int level = 0;
  
  	int rem_1 = 0;
	int rem_2 = 0;
	int rem_3 = 0;
	int flag = 0;
	int change_flag = 0;

	int *mem_ptr;
	
	void close_shm();
	
	    signal( SIGINT, (void*) close_shm);							//捕捉ctrl+c 信号
	seg_id = shmget(TIME_MEM_KEY, SEG_SIZE, IPC_CREAT|0777);
	if ( seg_id == -1)
		oops("shmget", 1);

	mem_ptr = shmat( seg_id, NULL, 0);
	if( mem_ptr == (void *) -1)
		oops("shmat", 2);


  /* 打开声音设备 */
  fd = open("/dev/dsp", O_RDWR);
  if (fd < 0) {
    perror("open of /dev/dsp failed");
    exit(1);
  }
  /* 设置采样时的量化位数 */
  arg = SIZE;
  status = ioctl(fd, SOUND_PCM_WRITE_BITS, &arg);
  if (status == -1)
    perror("SOUND_PCM_WRITE_BITS ioctl failed");
  if (arg != SIZE)
    perror("unable to set sample size");
  /* 设置采样时的声道数目 */
  arg = CHANNELS; 
  status = ioctl(fd, SOUND_PCM_WRITE_CHANNELS, &arg);
  if (status == -1)
    perror("SOUND_PCM_WRITE_CHANNELS ioctl failed");
  if (arg != CHANNELS)
    perror("unable to set number of channels");
  /* 设置采样时的采样频率 */
  arg = RATE;
  status = ioctl(fd, SOUND_PCM_WRITE_RATE, &arg);
  if (status == -1)
    perror("SOUND_PCM_WRITE_WRITE ioctl failed");
    
    
  /* 循环，直到按下Control-C */
 while (1) {
 //   printf("Say something:\n");
    status = read(fd, buf, sizeof(buf)); /* 录音 */
    if (status != sizeof(buf))
      perror("read wrong number of bytes");

  for(i=0;i<FFT_N;i++)                           //给结构体赋值
  {
     s[i].real=buf[i]; //实部为正弦波FFT_N点采样，赋值为1
     s[i].imag=0;                                //虚部为0
  }

//fft变换
  FFT(s);                                        //进行快速福利叶变换

  for(i=0;i<FFT_N;i++)                           //求变换后结果的模值，存入复数的实部部分
	{	
		s[i].real=s[i].real*s[i].real+s[i].imag*s[i].imag;
		value += 10*log(s[i].real);
	}
level = value / FFT_N;
printf("%d ",level );               //输出声音烈度
if (abs(level) > 30 && abs(level) < 8388592)
	change_flag = 1;
else
	change_flag = 0;
value = 0;

		rem_1 = rem_2;
		rem_2 = rem_3;
		rem_3 = change_flag;

		if( rem_1 + rem_2 + rem_3 > 2)
			flag = 1;
		if( rem_1 + rem_2 + rem_3 < 2)
			flag = 0;
	//printf("%d\n", flag);
	
	//strcpy(mem_ptr, flag);
	*mem_ptr = flag;
	printf("%d\n", *mem_ptr);
  }
	shmctl( seg_id, IPC_RMID, NULL);
	return flag;
}

void close_shm()
{
	shmctl( seg_id, IPC_RMID, NULL);
	exit(0);
}
