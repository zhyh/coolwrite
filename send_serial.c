
/*



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%                                                            %%%%%%
%%%%%%       欢迎到www.opencvchina.com下载源代码和资料              %%%%%%
%%%%%%                                                            %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




*/
/*
 * Code written by Lya (GeckoGeek.fr)
 */
 
#include "opencv/highgui.h"
#include "opencv/cv.h"
 
#include"linux_serial.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>     /*Unix标准函数定义*/
 /*the time server using shared memory */

#include<stdio.h>
#include<sys/shm.h>
#include<time.h>
#include<signal.h>
#include<math.h>

#define FALSE -1
#define TRUE 0

#define oops(m,x) {perror(m); exit(x);}

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))  
#define abs(x) ((x) > 0 ? (x) : -(x))
#define sign(x) ((x) > 0 ? 1 : -1)


/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%                                                            %%%%%%
%%%%%%       欢迎到www.opencvchina.com下载源代码和资料              %%%%%%
%%%%%%                                                            %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/
int main() {


	int serial_return;
	int fd;
	int write_flag = 0;
	int seek_num = 0;
	int x0 = 0;
	int y0 = 0;
	CvPoint objectPos = cvPoint(-1, -1);
	
	int read_num= 0;  //写入字节数
    
    FILE *fp = NULL; //需要注意
    fp = fopen("/tmp/serial", "r");
    if(NULL == fp)
    {
        return -1; //要返回错误代码
    }

	char buff[512];	//缓冲区
	char *dev ="/dev/ttyUSB0";	//usb转串口设备
	char *dev1 ="/dev/ttyS0";
	fd = OpenDev(dev);
	if (fd<0)
		fd = OpenDev(dev1);
	if (fd > 0)
		set_speed(fd,4800);
	else 
		{
		printf("Can't Open Serial Port!\n");
		exit(0);
		}
	 if (set_Parity(fd,8,1,'N')== FALSE)
  	{
    		printf("Set Parity Error\n");
    		exit(1);
  	}
  	while(1)
  	{
  		read_num = fscanf(fp, "%d %d %d ", &objectPos.x, &objectPos.y, &write_flag);  //依次读取临时文件中的数据
  		printf("%d %d %d \n", objectPos.x, objectPos.y, write_flag);
  		if(read_num != 3)
  		{
  		  	printf("read wrong%d\n",read_num);
  			fseek(fp, 0, SEEK_SET);
  			read_num = fscanf(fp, "%d %d %d ", &objectPos.x, &objectPos.y, &write_flag);  //依次读取临时文件中的数据
  		}

// 		objectPos.x = 100; objectPos.y = 40; write_flag = 1;
  		serial_return = send_serial(objectPos, write_flag, fd);  //通过串口发送数据
		if (serial_return <0)
			printf("send serial error");
		serial_return = read_serial(fd);  //收集单片机移动完成信号，否则等待VMINT 秒的时间u
		if (serial_return < 0)
			printf("move wrong\n");
			
			
  		while( write_flag == 0)  //越过标记为零的点
  			{ 
  				//seek_num = fseek(fp,10,SEEK_CUR);
 		  		//if(seek_num != 10)
//  				printf("seek wrong%d\n",objectPos.x);
  				read_num = fscanf(fp, "%d %d %d ", &objectPos.x, &objectPos.y, &write_flag);
//  				sleep(1);
  			}
			
	//sleep( max( abs(objectPos.x + objectPos.y - x0 -y0)/20, 0.5));
//	if(((abs(objectPos.x-x0) + abs(objectPos.y-y0))/20) > 2)
//		sleep((abs(objectPos.x-x0) + abs(objectPos.y-y0))/15);
//	else
//		sleep(2);

		x0 = objectPos.x;
		y0 = objectPos.y;
	}

 	close(fd);		//关闭文件描述符
 	fclose(fp);
 	
	return 0;
}

